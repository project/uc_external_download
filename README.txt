UBERCART EXTERNAL DOWNLOAD
--------------------------


CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

Adds external downloads as a product feature so an external url can be
downloaded after purchasing. This module is a direct copy of the uc_file
module modified to allow external file downloads.

The uc_file module allows you to set a file to be downloaded. But what if
you use a third party to serve files? This module allows you to add a URL
rather than a file for the user to download. The URL is never shown to the
user and is served just like normal files. Instead of download/123 an
external file uses download_url/123.

Uses the default download limits set in Store > Config > Products > File
download settings but can be overwritten at the product level.


REQUIREMENTS
------------

This module requires the following modules:

 * Ubercart (https://www.drupal.org/project/ubercart)


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * Works exactly like Ubercart Files except you put in a URL that the user
   can download.  External downloads are added as Ubercart features.

   To use:

   1. Edit a product and add the "External Download" feature
   2. Select the model
   3. Enter the URL to be downloaded
   4. Enter a filename (This can be the actual filename or a custom name you
      choose. eg. my_file.zip)