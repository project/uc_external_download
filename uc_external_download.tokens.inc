<?php

/**
 * @file
 * Token hooks for the uc_external_download module.
 */

/**
 * Implements hook_token_info().
 */
function uc_external_download_token_info() {
  $type = array(
    'name' => t('External file downloads'),
    'description' => t('Tokens for purchased external file downloads.'),
    'needs-data' => 'uc_external_download',
  );

  $tokens['downloads'] = array(
    'name' => t('Downloads'),
    'description' => t('The list of external file download links (if any) associated with an order'),
  );

  return array(
    'types' => array('uc_external_download' => $type),
    'tokens' => array('uc_external_download' => $tokens),
  );
}

/**
 * Implements hook_tokens().
 */
function uc_external_download_tokens($type, $tokens, $data = array(), $options = array()) {
  $language_code = NULL;
  if (isset($options['language'])) {
    $language_code = $options['language']->language;
  }
  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'uc_external_download' && !empty($data['uc_external_download'])) {
    $files = $data['uc_external_download'];

    if (isset($tokens['downloads'])) {
      $replacements[$tokens['downloads']] = theme('uc_external_download_downloads_token', array('file_downloads' => $files));
    }
  }

  return $replacements;
}

/**
 * Themes the file download links token.
 *
 * @ingroup themeable
 */
function theme_uc_external_download_downloads_token($variables) {
  $file_downloads = $variables['file_downloads'];

  $output = '';

  foreach ($file_downloads as $file_download) {
    $file_details = _uc_external_download_get_file_name_url($file_download->edid);
    $output .= l($file_details->filename, 'download_url/' . $file_download->edid, array('absolute' => TRUE)) . "\n";
  }

  return $output;
}
