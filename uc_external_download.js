/**
 * @file
 * Modifies the file selection and download access expiration interfaces.
 */

/**
 * Give visual feedback to the user about download numbers.
 *
 * Also popup download notice for external downloads.
 *
 * @param id int
 *   Download id.
 * @param accessed int
 *   Number of times accessed.
 * @param limit int
 *   Limit of accesses.
 */
function uc_external_download_update_download(id, accessed, limit) {
  if (accessed < limit || limit === -1) {

    // Handle the max download number as well.
    var downloads = '';
    downloads += accessed + 1;
    downloads += '/';
    downloads += limit === -1 ? 'Unlimited' : limit;
    jQuery('td#download-' + id).html(downloads);
    jQuery('td#download-' + id).attr('onclick', '');
  }

  // Popup overlay so the user lets the download start.
  // Popup goes away after 5 seconds.
  // Also helps with users that click links multiple times.
  if (!jQuery('#uc_external_download_overlay').length) {
    var overlay = jQuery('<div id="uc_external_download_overlay" style="position: fixed; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); z-index: 10000;"><div style="position: relative; margin: 300px auto auto; width: 200px; height: 75px; background-color: #fff; color: #000; text-align: center; padding: 20px 10px 0;">Please wait for download to start...</div></div>');
    overlay.appendTo(document.body);
  }

  else {
    jQuery('#uc_external_download_overlay').show();
  }
  window.setTimeout(function () {
    jQuery('#uc_external_download_overlay').hide();
  }, 5 * 1000);
}
