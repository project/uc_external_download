<?php

/**
 * @file
 * External download menu items.
 */

/**
 * The number of bogus requests one IP address can make before being banned.
 */
define('UC_EXTERNAL_DOWNLOAD_REQUEST_LIMIT', 50);

/**
 * Download file chunk.
 */
define('UC_EXTERNAL_DOWNLOAD_BYTE_SIZE', 8192);

/**
 * Download statuses.
 */
define('UC_EXTERNAL_DOWNLOAD_ERROR_OK', 0);
define('UC_EXTERNAL_DOWNLOAD_ERROR_TOO_MANY_LOCATIONS', 1);
define('UC_EXTERNAL_DOWNLOAD_ERROR_TOO_MANY_DOWNLOADS', 2);
define('UC_EXTERNAL_DOWNLOAD_ERROR_EXPIRED', 3);
define('UC_EXTERNAL_DOWNLOAD_ERROR_HOOK_ERROR', 4);

/**
 * Start download process.
 */
function _uc_external_download_download($edid) {
  global $user;

  // Error messages for various failed download states.
  $admin_message = t('Please contact the site administrator if this message has been received in error.');
  $error_messages = array(
    UC_EXTERNAL_DOWNLOAD_ERROR_TOO_MANY_LOCATIONS      => t('You have downloaded this file from too many different locations.'),
    UC_EXTERNAL_DOWNLOAD_ERROR_TOO_MANY_DOWNLOADS      => t('You have reached the download limit for this file.'),
    UC_EXTERNAL_DOWNLOAD_ERROR_EXPIRED                 => t('This file download has expired.') . ' ',
    UC_EXTERNAL_DOWNLOAD_ERROR_HOOK_ERROR              => t('A hook denied your access to this file.') . ' ',
  );

  $ip = ip_address();
  if (!user_access('view all downloads')) {

    $query = db_select('uc_external_download_users', 'u')
      ->condition('u.uid', $user->uid)
      ->condition('u.edid', $edid);
    $query->leftJoin('uc_external_download', 'f', 'f.edid = u.edid');
    $query->fields('u', array(
      'fuid',
      'edid',
      'uid',
      'pfid',
      'file_key',
      'granted',
      'expiration',
      'accessed',
      'addresses',
      'download_limit',
      'address_limit',
    ))->fields('f', array(
      'filename',
      'url',
    ));
    $result = $query->execute()->fetchObject();

    if ($result) {
      $result->addresses = unserialize($result->addresses);
    }

  }
  else {
    $result = db_query("SELECT * FROM {uc_external_download} WHERE edid = :edid", array(':edid' => $edid))->fetchObject();
  }

  if (!$result) {
    return MENU_ACCESS_DENIED;
  }

  // If it's ok, we push the file to the user.
  $status = UC_EXTERNAL_DOWNLOAD_ERROR_OK;
  if (!user_access('view all downloads')) {
    $status = _uc_external_download_validate($result, $user, $ip);
  }
  if ($status == UC_EXTERNAL_DOWNLOAD_ERROR_OK) {
    _uc_external_download_transfer($result, $ip);
  }

  // Some error state came back, so report it.
  else {
    drupal_set_message($error_messages[$status] . $admin_message, 'error');

    // Kick 'em to the curb. >:)
    _uc_external_download_redirect($user->uid);
  }

  drupal_exit();
}

/**
 * Performs first-pass authorization. Calls authorization hooks afterwards.
 *
 * Called when a user requests a external file download, function
 * checks download limits.  Passing that, the function
 * _uc_external_download_transfer() is called.
 */
function _uc_external_download_validate($file_download, &$user, $ip) {
  $message_user = ($user->uid) ? t('The user %username', array('%username' => format_username($user))) : t('The IP address %ip', array('%ip' => $ip));

  $addresses = $file_download->addresses;

  // Check the number of locations.
  if (!empty($file_download->address_limit) && !in_array($ip, $addresses) && count($addresses) >= $file_download->address_limit) {
    // $message_user has already been passed through check_plain()
    watchdog('uc_external_download', '!username has been denied a external file download by downloading it from too many IP addresses.', array('!username' => $message_user), WATCHDOG_WARNING);

    return UC_EXTERNAL_DOWNLOAD_ERROR_TOO_MANY_LOCATIONS;
  }

  // Check the downloads so far.
  if (!empty($file_download->download_limit) && $file_download->accessed >= $file_download->download_limit) {
    // $message_user has already been passed through check_plain()
    watchdog('uc_external_download', '!username has been denied a file download by downloading it too many times.', array('!username' => $message_user), WATCHDOG_WARNING);

    return UC_EXTERNAL_DOWNLOAD_ERROR_TOO_MANY_DOWNLOADS;
  }

  // Check if it's expired.
  if ($file_download->expiration && REQUEST_TIME >= $file_download->expiration) {
    // $message_user has already been passed through check_plain()
    watchdog('uc_external_download', '!username has been denied an expired file download.', array('!username' => $message_user), WATCHDOG_WARNING);

    return UC_EXTERNAL_DOWNLOAD_ERROR_EXPIRED;
  }

  // Check any if any hook_uc_download_authorize() calls deny the download.
  foreach (module_implements('uc_download_authorize') as $module) {
    $name = $module . '_uc_download_authorize';
    $result = $name($user, $file_download);
    if (!$result) {
      return UC_EXTERNAL_DOWNLOAD_ERROR_HOOK_ERROR;
    }
  }

  // Everything's ok!
  // $message_user has already been passed through check_plain()
  watchdog('uc_external_download', '!username has started download of the external file %filename.', array('!username' => $message_user, '%filename' => basename($file_download->filename)), WATCHDOG_NOTICE);
}

/**
 * Sends the external file to a user via HTTP and updates the database.
 *
 * @param $file_user
 *   The file_user object from the uc_external_download_users.
 * @param $ip
 *   The string containing the IP address the download is going to.
 */
function _uc_external_download_transfer($file_user, $ip) {
  $file_url = $file_user->url;

  $data = file_get_contents($file_url);

  // Get headers from file request and put them into a useable array.
  $file_headers = array_change_key_case(_uc_external_download_parse_headers($http_response_header));

  // We can't be sure what the headers will tell us...
  // But lets check a few basics.
  // Content-Type.
  if (!isset($file_headers['content-type']) || empty($file_headers['content-type'])) {
    watchdog('uc_external_download', 'External download url returns empty or no Content-Type: !url', array('!url' => $file_url), WATCHDOG_WARNING);
  }

  // Content-Length.
  if (!isset($file_headers['content-length']) || empty($file_headers['content-length']) || $file_headers['content-length'] == 0) {
    watchdog('uc_external_download', 'External download url returns empty, none or 0 Content-Length: !url', array('!url' => $file_url), WATCHDOG_WARNING);
  }

  header('Content-Description: File Transfer');
  header('Cache-Control: no-store, no-cache, must-revalidate');
  header('Cache-Control: post-check=0, pre-check=0', FALSE);
  header('Pragma: no-cache');
  // Only cache for 2 hours.
  header('Expires: ' . gmdate("D, d M Y H:i:s", mktime(date("H") + 2, date("i"), date("s"), date("m"), date("d"), date("Y"))) . ' GMT');
  header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
  header('Content-Type: ' . $file_headers['content-type']);
  header('Content-Length: ' . $file_headers['content-length']);
  header('Content-Disposition: attachment; filename=' . $file_user->filename);
  header('Content-Transfer-Encoding: binary\n');

  echo $data;

  // Log this download.
  if (!user_access('view all downloads')) {
    _uc_external_download_log_download($file_user, $ip);
  }
}

/**
 * Processes a file download.
 */
function _uc_external_download_log_download($file_user, $ip) {

  // Add the address if it doesn't exist.
  $addresses = $file_user->addresses;
  if (!in_array($ip, $addresses)) {
    $addresses[] = $ip;
  }
  $file_user->addresses = $addresses;

  // Accessed again.
  $file_user->accessed++;

  // Calculate hash.
  $file_user->file_key = drupal_get_token(serialize($file_user));

  drupal_write_record('uc_external_download_users', $file_user, 'fuid');
}

/**
 * Send 'em packin.
 */
function _uc_external_download_redirect($uid = NULL) {

  // Shoo away anonymous users.
  if ($uid == 0) {
    drupal_access_denied();
  }
  // Redirect users back to their file page.
  else {
    if (!headers_sent()) {
      drupal_goto('user/' . $uid . '/purchased-files');
    }
  }
}

/**
 * Parse headers.
 */
function _uc_external_download_parse_headers($headers) {
  $head = array();
  foreach ($headers as $k => $v) {
    $t = explode(':', $v, 2);
    if (isset($t[1])) {
      $head[trim($t[0])] = trim($t[1]);
    }
    else {
      $head[] = $v;
      if (preg_match("#HTTP/[0-9\.]+\s+([0-9]+)#", $v, $out)) {
        $head['reponse_code'] = intval($out[1]);
      }
    }
  }
  return $head;
}
